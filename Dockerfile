#Docker file changes 
FROM node:14 AS builder

ADD package*.json /app/
WORKDIR /app
RUN npm install

ADD . /app
RUN npm run-script build

FROM nginx:1.17-alpine
COPY ./index.html /usr/share/nginx/html/html

